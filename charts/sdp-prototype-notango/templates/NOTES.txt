SKA SDP prototype deployed!
{{- if .Values.console.enabled }}

You can connect to the configuration database by running a shell in the
console pod. To start a bash shell, use:

    $ kubectl exec -it deploy/{{ include "sdp-prototype.fullname" . }}-console -- bash

and from there you can use the sdpcfg command, e.g.:

    # sdpcfg ls -R /

Alternatively to start an iPython shell, use:

    $ kubectl exec -it deploy/{{ include "sdp-prototype.fullname" . }}-console -- ipython

and from there you can use the ska_sdp_config package, e.g.:

    import ska_sdp_config
    config = ska_sdp_config.Config()

{{- end }}
