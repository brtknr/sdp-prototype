Test Workflows
==============

.. toctree::
  :maxdepth: 1

  test_realtime
  test_batch
  test_receive_addresses
  test_dask
  test_daliuge
